package com.fusion.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fusion.utils.CommonUtils;

@RestController
@RequestMapping("/v1/security")
public class SecurityController
{

	@Autowired
	private Environment environment;

	@Autowired
	CommonUtils utils;

	@GetMapping(path = "/", produces = "application/json")
	public ResponseEntity<String> getSecurityToken()
	{
		return new ResponseEntity<>("This is security token ", HttpStatus.OK);
	}
}
