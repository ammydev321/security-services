package com.fusion.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FusionSecuritySuiteApplication
{

	public static void main(String[] args)
	{
		SpringApplication.run(FusionSecuritySuiteApplication.class, args);
	}

}
